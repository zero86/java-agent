package agent;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;

public class PreMainTraceAgent {
    public static void premain(String agentArgs, Instrumentation ins){
        System.out.println("agentArgs:"+agentArgs);
        ins.addTransformer(new DefineTransformer(), true);
    }

    public static void agentmain(String agentArgs, Instrumentation ins){
        System.out.println("agentArgs:"+agentArgs);
        ins.addTransformer(new DefineTransformer(), true);
    }

    public static class DefineTransformer implements ClassFileTransformer{
        @Override
        public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
            System.out.println("premain load Class:"+className);
            return classfileBuffer;
        }
    }
}
